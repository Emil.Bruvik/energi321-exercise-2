import matplotlib.pyplot as plt
import numpy as np
import os
import shutil
import scipy.io as sio 
import glob
import re
import pandas as pd
import platform  # Import platform module

filename = 'data_SNII.mat'

matlab_data = sio.loadmat(filename)

Cp = matlab_data['Cp']
U = matlab_data['U']

N = len(Cp)
air_density = 1.225
radius = 62.575
max_power = 5
power = np.linspace(0, N, N)

for t in range(N):
  power[t] = 1/2 * np.pi * air_density * radius**2 * Cp[t] * U[t]**3

power_year = sum(power) / (1e6 * 5 * 8760)
print('Power output: ', power_year, 'MW')
  
capacity_factor = (power_year / max_power) * 100
print('Capacity factor: ', capacity_factor, '%')