import matplotlib.pyplot as plt
import numpy as np
import os
import shutil
import scipy.io as sio 
import glob
import re
import pandas as pd
import platform  # Import platform module

filename = 'data_SNII.mat'

matlab_data = sio.loadmat(filename)

print("Variables stored in the Matlab data file:")
for key in matlab_data.keys():
  print(key)

Cp = matlab_data['Cp']
U = matlab_data['U']

Cp_low = Cp[U<4]
Cp_high = Cp[U>25]

np.set_printoptions(threshold=np.inf, precision=3, suppress=True)

print(Cp_high)
print(Cp_low)