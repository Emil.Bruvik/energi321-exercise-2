import matplotlib.pyplot as plt
import numpy as np
import os
import shutil
import glob
import re
import pandas as pd
import platform  # Import platform module

plt.style.use('./custom_latex_style.mplstyle')

filename = 'driver_EX1.dvr'

df = pd.read_csv(filename, skiprows=20, delim_whitespace=True)

print(df.columns)

wndspeed = df.iloc[:, 0].tolist()
rotspd = df.iloc[:, 2].tolist()
pitch = df.iloc[:, 3].tolist()

print("Wind Speed:", wndspeed[:5])
print("Rotor Speed:", rotspd[:5])
print("Pitch:", pitch[:5])

plt.figure(figsize=(10, 6))
plt.plot(wndspeed, pitch, 'o-', color='black')
plt.title('Pitch angle Curve')
plt.xlabel('Wind Speed (m/s)')
plt.ylabel('Pitch Angle (deg)')
plt.grid(True)
plt.savefig('pitchanglecurve.pdf')

plt.figure(figsize=(10, 6))
plt.plot(wndspeed, rotspd, 'o-', color='brown')
plt.title('Rotor Speed Curve')
plt.xlabel('Wind Speed (m/s)')
plt.ylabel('Rotor Speed (RPM)')
plt.grid(True)
plt.savefig('rotorspeedcurve.pdf')